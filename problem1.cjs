const carsInventory = require('./carsdata.cjs');

function findCarById(carsInventory,id){

    //checking edge case if inventory is empty 
    if(!carsInventory || carsInventory.length === 0 || Array.isArray(carsInventory) === false){
        return [];
    }
    //checking edge cases for the car id
    if(typeof id !== 'number'){
        return [];
    }
    let result;
    for(let index = 0; index < carsInventory.length; index++){
        let cars = carsInventory[index];
        if(cars.id === id){
            result = cars;
        }
    }    
    return result;
}
module.exports = findCarById;



