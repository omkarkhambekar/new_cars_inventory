const carsInventory = require('./carsdata.cjs');
function sortedCarsByModel(carsInventory){
    let result = carsInventory.sort(function compareFn(a, b) {
        if (a.car_model.toLowerCase() <  b.car_model.toLowerCase()){
          return -1;
        }
        if (a.car_model.toLowerCase() >  b.car_model.toLowerCase()){
          return 1;
        }
        return 0;
    });
    return result;
}
let res = sortedCarsByModel(carsInventory);
module.exports = res;